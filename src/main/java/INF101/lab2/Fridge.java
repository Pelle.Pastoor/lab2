package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    List<FridgeItem> items = new ArrayList<>();
    int sizeFridge = 20; 

	public int nItemsInFridge(){
        System.out.println(this.items.size());
        return this.items.size();
    }

	public int totalSize(){
        return this.sizeFridge;
    }

	public boolean placeIn(FridgeItem item){
        if (this.totalSize() == this.nItemsInFridge()){
            return false;
        }else {
            return this.items.add(item);
        }
    }
    
	public void takeOut(FridgeItem item){
        if (this.items.contains(item)){
            this.items.remove(item);
        }else{
            throw new NoSuchElementException("Item doesn't exist!");
        }
    }
    
	public void emptyFridge(){
        this.items.clear();
    }
    
	public List<FridgeItem> removeExpiredFood(){
        List<FridgeItem> expired = new ArrayList<>();
        for (int i = this.items.size()-1; i >= 0; i--) {
            if(this.items.get(i).hasExpired()){
                expired.add(this.items.get(i));
                this.items.remove(i);
            }
        }
        return expired;
    }
}
